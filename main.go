package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var (
	directory        string
	iteratorFileName string
	port             string
	pwd              string
)

// Note name is hex number

func initStorage() {
	err := os.MkdirAll(directory, os.ModeDir|os.ModePerm)
	if err != nil {
		log.Fatal("error: cannot create a storage directory or access to it:", err)
	}

	_ = getLastName(directory + "/" + iteratorFileName)
}

var antiInfinitiveRecursion = 0

func getLastName(filename string) uint64 {
	antiInfinitiveRecursion++
	if antiInfinitiveRecursion > 5 {
		log.Fatalln("fall in infinitive recursion")
	}
	iteratorFile, err := os.Open(filename)
	if os.IsNotExist(err) {
		iteratorFile, err := os.Create(filename)
		if err != nil {
			log.Fatalln("error: cannot create the iterator file:", err)
		}
		_, err = iteratorFile.Write([]byte{'0'})
		if err != nil {
			log.Fatalln("error: cannot write zero in the iterator file:", err)
		}
		err = iteratorFile.Close()
		if err != nil {
			log.Println("error closing the iterator file:", err)
		}
		return getLastName(filename)
	} else if err != nil {
		log.Fatalln("error opening the iterator file:", err)
	}
	iteratorFileData := make([]byte, 64)
	readLength, err := iteratorFile.Read(iteratorFileData)
	if err != nil {
		log.Fatal("cannot read the iterator file:", err)
	}
	stringValue := string(iteratorFileData[:readLength])
	var value uint64
	scanned, err := fmt.Sscanf(stringValue, "%x", &value)
	if err != nil {
		log.Fatal("error: illegal value in iterator file:", err)
	}
	if scanned == 0 {
		log.Fatalln("error: no value was read for the iterator file")
	}
	err = iteratorFile.Close()
	if err != nil {
		log.Println("error closing the iterator file:", err)
	}
	antiInfinitiveRecursion = 0
	return value
}

// Gets last name, gets it up and returns it
func iterate() uint64 {
	value := getLastName(directory + "/" + iteratorFileName)
	value++

	// write new value to the iterator file
	f, err := os.OpenFile(directory+"/"+iteratorFileName, os.O_WRONLY, os.ModePerm)
	if err != nil {
		log.Fatalln("error: cannot open the iterator file")
	}
	_, err = f.Write([]byte(fmt.Sprintf("%x", value)))
	if err != nil {
		log.Fatalln("error iterating the iterator file:", err)
	}
	err = f.Close()
	if err != nil {
		log.Println("error closing the iterator file")
	}
	return value
}

// return an answer. It can be an error or an written note name if success
func writeNote(note []byte) string {
	noteName := fmt.Sprintf("%x", iterate())

	f, err := os.Create(directory + "/" + noteName)
	if err != nil {
		const e = "error creating new note file"
		log.Println(e)
		return e
	}
	_, err = f.Write(note)
	if err != nil {
		const e = "error writing data to new note file"
		log.Println(e)
		return e
	}

	return noteName
}

func initServer() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if pwd != r.Header.Get("PWD") {
			errMsg := "error: unauthorized\n"
			log.Print(errMsg)
			w.Write([]byte(errMsg))
			return
		}

		switch r.Method {
		case http.MethodGet:
			noteName := r.URL.Path[1:]
			data, err := ioutil.ReadFile(directory + "/" + noteName)
			if err != nil {
				errMsg := fmt.Sprintf("error reading note %s: %s\n", noteName, err)
				if os.IsNotExist(err) {
					errMsg = "not found\n"
				}
				log.Print(errMsg)
				w.Write([]byte(errMsg))
			}
			w.Write(data)
		case http.MethodPost:
			data, err := ioutil.ReadAll(r.Body)
			if len(data) == 0 {
				errMsg := "error saving note: no data received"
				log.Print(errMsg)
				w.Write([]byte(errMsg))
				return
			}
			if err != nil {
				errMsg := fmt.Sprintf("error reading an existing body: %s\n", err)
				log.Print(errMsg)
				w.Write([]byte(errMsg))
				return
			}
			postedNoteName := writeNote(data)
			w.Write([]byte(postedNoteName))
		}
	})
}

func main() {
	flag.StringVar(&port, "port", "8080", "port to start server on")
	flag.StringVar(&directory, "dir", "storage", "storage directory")
	flag.StringVar(&pwd, "pwd", "secret123", "password, use with curl: --header \"PWD: secret123\"")
	flag.StringVar(&iteratorFileName, "iter", ".iterator", "iterator file is used to name notes and is in storage directory")
	flag.Parse()

	initStorage()

	initServer()

	log.Printf("starting web server on port %s\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
